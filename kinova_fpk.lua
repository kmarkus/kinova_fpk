return block 
{
      name = "kinova_fpk",
      license = "LGPL-3.0",
      meta_data = "",
      port_cache = true,

      types = {},

      configurations = {},

      ports = {
	 { name = "jnt_pos", in_type_name = "double", in_data_len = 7 },
	 { name = "poses", out_type_name = "struct gc_pose", out_data_len = 7 },
      },

      operations = { start=false, stop=false, step=true }
}
