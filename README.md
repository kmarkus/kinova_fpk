kinova foward kinematics block packaged from the example
`sim_kinova_gen3_bullet.c example` of the vericomp-application.

License: LGPL-3.0
