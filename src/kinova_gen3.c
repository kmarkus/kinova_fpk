#include <math.h>
#include "kinova_gen3.h"

// Remember that for spatial transforms (as implemented in this macro) the
// rotation matrix is inverted w.r.t. homogeneous transforms
#define TAIT_BRYAN_ZYX_INTRINSIC(yaw, pitch, roll) \
{ \
    .row_x = {                         cos(yaw) * cos(pitch)          ,                          sin(yaw) * cos(pitch)          ,       -sin(pitch)     }, \
    .row_y = {cos(yaw) * sin(pitch) * sin(roll) - sin(yaw) * cos(roll), sin(yaw) * sin(pitch) * sin(roll) + cos(yaw) * cos(roll), cos(pitch) * sin(roll)}, \
    .row_z = {cos(yaw) * sin(pitch) * cos(roll) + sin(yaw) * sin(roll), sin(yaw) * sin(pitch) * cos(roll) - cos(yaw) * sin(roll), cos(pitch) * cos(roll)}  \
}

#define TAIT_BRYAN_XYZ_EXTRINSIC(roll, pitch, yaw) \
    TAIT_BRYAN_ZYX_INTRINSIC(yaw, pitch, roll)


struct kinova_gen3
{
    struct frame base_link_root;
    struct frame base_link_tip;
    struct body base_link;
    struct frame Shoulder_Link_root;
    struct frame Shoulder_Link_tip;
    struct body Shoulder_Link;
    struct frame HalfArm1_Link_root;
    struct frame HalfArm1_Link_tip;
    struct body HalfArm1_Link;
    struct frame HalfArm2_Link_root;
    struct frame HalfArm2_Link_tip;
    struct body HalfArm2_Link;
    struct frame ForeArm_Link_root;
    struct frame ForeArm_Link_tip;
    struct body ForeArm_Link;
    struct frame SphericalWrist1_Link_root;
    struct frame SphericalWrist1_Link_tip;
    struct body SphericalWrist1_Link;
    struct frame SphericalWrist2_Link_root;
    struct frame SphericalWrist2_Link_tip;
    struct body SphericalWrist2_Link;
    struct frame Bracelet_Link_root;
    struct frame Bracelet_Link_tip;
    struct body Bracelet_Link;
    struct frame EndEffector_Link_root;
    struct frame EndEffector_Link_tip;
    struct body EndEffector_Link;

    struct frame Actuator1_frame;
    struct frame Actuator2_frame;
    struct frame Actuator3_frame;
    struct frame Actuator4_frame;
    struct frame Actuator5_frame;
    struct frame Actuator6_frame;
    struct frame Actuator7_frame;
};

static struct kinova_gen3 kinova_gen3 = {
    .base_link_root = { .name = "base_link_root" },
    .base_link_tip= { .name = "base_link_tip" },
    .base_link = { .name = "base_link" },
    .Shoulder_Link_root = { .name = "Shoulder_Link_root" },
    .Shoulder_Link_tip= { .name = "Shoulder_Link_tip" },
    .Shoulder_Link = { .name = "Shoulder_Link" },
    .HalfArm1_Link_root = { .name = "HalfArm1_Link_root" },
    .HalfArm1_Link_tip= { .name = "HalfArm1_Link_tip" },
    .HalfArm1_Link = { .name = "HalfArm1_Link" },
    .HalfArm2_Link_root = { .name = "HalfArm2_Link_root" },
    .HalfArm2_Link_tip= { .name = "HalfArm2_Link_tip" },
    .HalfArm2_Link = { .name = "HalfArm2_Link" },
    .ForeArm_Link_root = { .name = "ForeArm_Link_root" },
    .ForeArm_Link_tip= { .name = "ForeArm_Link_tip" },
    .ForeArm_Link = { .name = "ForeArm_Link" },
    .SphericalWrist1_Link_root = { .name = "SphericalWrist1_Link_root" },
    .SphericalWrist1_Link_tip= { .name = "SphericalWrist1_Link_tip" },
    .SphericalWrist1_Link = { .name = "SphericalWrist1_Link" },
    .SphericalWrist2_Link_root = { .name = "SphericalWrist2_Link_root" },
    .SphericalWrist2_Link_tip= { .name = "SphericalWrist2_Link_tip" },
    .SphericalWrist2_Link = { .name = "SphericalWrist2_Link" },
    .Bracelet_Link_root = { .name = "Bracelet_Link_root" },
    .Bracelet_Link_tip= { .name = "Bracelet_Link_tip" },
    .Bracelet_Link = { .name = "Bracelet_Link" },
    .EndEffector_Link_root = { .name = "EndEffector_Link_root" },
    .EndEffector_Link_tip= { .name = "EndEffector_Link_tip" },
    .EndEffector_Link = { .name = "EndEffector_Link" },

    .Actuator1_frame = { .name = "Actuator1_frame" },
    .Actuator2_frame = { .name = "Actuator2_frame" },
    .Actuator3_frame = { .name = "Actuator3_frame" },
    .Actuator4_frame = { .name = "Actuator4_frame" },
    .Actuator5_frame = { .name = "Actuator5_frame" },
    .Actuator6_frame = { .name = "Actuator6_frame" },
    .Actuator7_frame = { .name = "Actuator7_frame" },
};

struct kca_segment kinova_gen3_segments_a[] = {
    {
        .joint_attachment = {
            .target_frame = &kinova_gen3.base_link_tip,
            .target_body = &kinova_gen3.base_link,
            .reference_frame = &kinova_gen3.base_link_root,
            .reference_body = &kinova_gen3.base_link
        },
        .joint = {
            .target_frame = &kinova_gen3.Shoulder_Link_root,
            .target_body = &kinova_gen3.Shoulder_Link,
            .reference_frame = &kinova_gen3.base_link_tip,
            .reference_body = &kinova_gen3.base_link
        },
        .link = {
            .root_frame = &kinova_gen3.Shoulder_Link_root,
            .inertia = {
                .frame = &kinova_gen3.Shoulder_Link_root,
                .body = &kinova_gen3.Shoulder_Link,
            }
        }
    },
    {
        .joint_attachment = {
            .target_frame = &kinova_gen3.Shoulder_Link_tip,
            .target_body = &kinova_gen3.Shoulder_Link,
            .reference_frame = &kinova_gen3.Shoulder_Link_root,
            .reference_body = &kinova_gen3.Shoulder_Link
        },
        .joint = {
            .target_frame = &kinova_gen3.HalfArm1_Link_root,
            .target_body = &kinova_gen3.HalfArm1_Link,
            .reference_frame = &kinova_gen3.Shoulder_Link_tip,
            .reference_body = &kinova_gen3.Shoulder_Link
        },
        .link = {
            .root_frame = &kinova_gen3.HalfArm1_Link_root,
            .inertia = {
                .frame = &kinova_gen3.HalfArm1_Link_root,
                .body = &kinova_gen3.HalfArm1_Link,
            }
        }
    },
    {
        .joint_attachment = {
            .target_frame = &kinova_gen3.HalfArm1_Link_tip,
            .target_body = &kinova_gen3.HalfArm1_Link,
            .reference_frame = &kinova_gen3.HalfArm1_Link_root,
            .reference_body = &kinova_gen3.HalfArm1_Link
        },
        .joint = {
            .target_frame = &kinova_gen3.HalfArm2_Link_root,
            .target_body = &kinova_gen3.HalfArm2_Link,
            .reference_frame = &kinova_gen3.HalfArm1_Link_tip,
            .reference_body = &kinova_gen3.HalfArm1_Link
        },
        .link = {
            .root_frame = &kinova_gen3.HalfArm2_Link_root,
            .inertia = {
                .frame = &kinova_gen3.HalfArm2_Link_root,
                .body = &kinova_gen3.HalfArm2_Link,
            }
        }
    },
    {
        .joint_attachment = {
            .target_frame = &kinova_gen3.HalfArm2_Link_tip,
            .target_body = &kinova_gen3.HalfArm2_Link,
            .reference_frame = &kinova_gen3.HalfArm2_Link_root,
            .reference_body = &kinova_gen3.HalfArm2_Link
        },
        .joint = {
            .target_frame = &kinova_gen3.ForeArm_Link_root,
            .target_body = &kinova_gen3.ForeArm_Link,
            .reference_frame = &kinova_gen3.HalfArm2_Link_tip,
            .reference_body = &kinova_gen3.HalfArm2_Link
        },
        .link = {
            .root_frame = &kinova_gen3.ForeArm_Link_root,
            .inertia = {
                .frame = &kinova_gen3.ForeArm_Link_root,
                .body = &kinova_gen3.ForeArm_Link,
            }
        }
    },
    {
        .joint_attachment = {
            .target_frame = &kinova_gen3.ForeArm_Link_tip,
            .target_body = &kinova_gen3.ForeArm_Link,
            .reference_frame = &kinova_gen3.ForeArm_Link_root,
            .reference_body = &kinova_gen3.ForeArm_Link
        },
        .joint = {
            .target_frame = &kinova_gen3.SphericalWrist1_Link_root,
            .target_body = &kinova_gen3.SphericalWrist1_Link,
            .reference_frame = &kinova_gen3.ForeArm_Link_tip,
            .reference_body = &kinova_gen3.ForeArm_Link
        },
        .link = {
            .root_frame = &kinova_gen3.SphericalWrist1_Link_root,
            .inertia = {
                .frame = &kinova_gen3.SphericalWrist1_Link_root,
                .body = &kinova_gen3.SphericalWrist1_Link,
            }
        }
    },
    {
        .joint_attachment = {
            .target_frame = &kinova_gen3.SphericalWrist1_Link_tip,
            .target_body = &kinova_gen3.SphericalWrist1_Link,
            .reference_frame = &kinova_gen3.SphericalWrist1_Link_root,
            .reference_body = &kinova_gen3.SphericalWrist1_Link
        },
        .joint = {
            .target_frame = &kinova_gen3.SphericalWrist2_Link_root,
            .target_body = &kinova_gen3.SphericalWrist2_Link,
            .reference_frame = &kinova_gen3.SphericalWrist1_Link_tip,
            .reference_body = &kinova_gen3.SphericalWrist1_Link
        },
        .link = {
            .root_frame = &kinova_gen3.SphericalWrist2_Link_root,
            .inertia = {
                .frame = &kinova_gen3.SphericalWrist2_Link_root,
                .body = &kinova_gen3.SphericalWrist2_Link,
            }
        }
    },
    {
        .joint_attachment = {
            .target_frame = &kinova_gen3.SphericalWrist2_Link_tip,
            .target_body = &kinova_gen3.SphericalWrist2_Link,
            .reference_frame = &kinova_gen3.SphericalWrist2_Link_root,
            .reference_body = &kinova_gen3.SphericalWrist2_Link
        },
        .joint = {
            .target_frame = &kinova_gen3.Bracelet_Link_root,
            .target_body = &kinova_gen3.Bracelet_Link,
            .reference_frame = &kinova_gen3.SphericalWrist2_Link_tip,
            .reference_body = &kinova_gen3.SphericalWrist2_Link
        },
        .link = {
            .root_frame = &kinova_gen3.Bracelet_Link_root,
            .inertia = {
                .frame = &kinova_gen3.Bracelet_Link_root,
                .body = &kinova_gen3.Bracelet_Link,
            }
        }
    },
};

struct kcc_segment kinova_gen3_segments_c[] = {
    {
        .joint_attachment = {
            .rotation = (struct matrix3x3 [1]) { { TAIT_BRYAN_XYZ_EXTRINSIC(3.1416, 2.7629e-18, -4.9305e-36) } },
            .translation = (struct vector3 [1]) {0.0, 0.0, 0.15643},
        },
        .joint = {
            .type = JOINT_TYPE_REVOLUTE,
            .revolute_joint = {
                .axis = JOINT_AXIS_Z,
                .inertia = (double[]) { 0.0 }
            }
        },
        .link = {
            .inertia = {
                .zeroth_moment_of_mass = 1.3773,
                .first_moment_of_mass = {-3.16779e-05, -0.014274337199999999, -0.101038728},
                .second_moment_of_mass = {
                    .row_x = { 0.006294261000000001, 1.3773e-06, 2.7546e-06 },
                    .row_y = { 1.3773e-06, 0.0066537363, 0.0006170304 },
                    .row_z = { 2.7546e-06, 0.0006170304, 0.0019406157000000002 }
                }
            },
        }
    },
    {
        .joint_attachment = {
            .rotation = (struct matrix3x3 [1]) { { TAIT_BRYAN_XYZ_EXTRINSIC(1.5708, 2.1343e-17, -1.1102e-16) } },
            .translation = (struct vector3 [1]) {0.0, 0.005375, -0.12838},
        },
        .joint = {
            .type = JOINT_TYPE_REVOLUTE,
            .revolute_joint = {
                .axis = JOINT_AXIS_Z,
                .inertia = (double[]) { 0.0 }
            }
        },
        .link = {
            .inertia = {
                .zeroth_moment_of_mass = 1.1636,
                .first_moment_of_mass = {-5.1198399999999994e-05, -0.115871288, -0.015450280799999999},
                .second_moment_of_mass = {
                    .row_x = { 0.012901996800000001, 5.818e-06, 0.0 },
                    .row_y = { 5.818e-06, 0.0012473792, -0.0008040476 },
                    .row_z = { 0.0, -0.0008040476, 0.013096318 }
                }
            },
        }
    },
    {
        .joint_attachment = {
            .rotation = (struct matrix3x3 [1]) { { TAIT_BRYAN_XYZ_EXTRINSIC(-1.5708, 1.2326e-32, -2.9122e-16) } },
            .translation = (struct vector3 [1]) {0.0, -0.21038, -0.006375},
        },
        .joint = {
            .type = JOINT_TYPE_REVOLUTE,
            .revolute_joint = {
                .axis = JOINT_AXIS_Z,
                .inertia = (double[]) { 0.0 }
            }
        },
        .link = {
            .inertia = {
                .zeroth_moment_of_mass = 1.1636,
                .first_moment_of_mass = {-5.1198399999999994e-05, -0.0077274676, -0.1371791312},
                .second_moment_of_mass = {
                    .row_x = { 0.012720475200000001, 0.0, -8.1452e-06 },
                    .row_y = { 0.0, 0.012947377199999999, 0.0007051415999999999 },
                    .row_z = { -8.1452e-06, 0.0007051415999999999, 0.0012136348000000002 }
                }
            },
        }
    },
    {
        .joint_attachment = {
            .rotation = (struct matrix3x3 [1]) { { TAIT_BRYAN_XYZ_EXTRINSIC(1.5708, -6.6954e-17, -1.6653e-16) } },
            .translation = (struct vector3 [1]) {0.0, 0.006375, -0.21038},
        },
        .joint = {
            .type = JOINT_TYPE_REVOLUTE,
            .revolute_joint = {
                .axis = JOINT_AXIS_Z,
                .inertia = (double[]) { 0.0 }
            }
        },
        .link = {
            .inertia = {
                .zeroth_moment_of_mass = 0.9302,
                .first_moment_of_mass = {-1.67436e-05, -0.0702096356, -0.0139585812},
                .second_moment_of_mass = {
                    .row_x = { 0.0075783394, -9.302e-07, 0.0 },
                    .row_y = { -9.302e-07, 0.0005869562000000001, -0.00046510000000000003 },
                    .row_z = { 0.0, -0.00046510000000000003, 0.007735543200000001 }
                }
            },
        }
    },
    {
        .joint_attachment = {
            .rotation = (struct matrix3x3 [1]) { { TAIT_BRYAN_XYZ_EXTRINSIC(-1.5708, 2.2204e-16, -6.373e-17) } },
            .translation = (struct vector3 [1]) {0.0, -0.20843, -0.006375},
        },
        .joint = {
            .type = JOINT_TYPE_REVOLUTE,
            .revolute_joint = {
                .axis = JOINT_AXIS_Z,
                .inertia = (double[]) { 0.0 }
            }
        },
        .link = {
            .inertia = {
                .zeroth_moment_of_mass = 0.6781,
                .first_moment_of_mass = {6.781e-07, -0.0063958392, -0.0433190623},
                .second_moment_of_mass = {
                    .row_x = { 0.0010822476000000001, 0.0, 0.0 },
                    .row_y = { 0.0, 0.0010897067, 0.0001735936 },
                    .row_z = { 0.0, 0.0001735936, 0.00027056190000000004 }
                }
            },
        }
    },
    {
        .joint_attachment = {
            .rotation = (struct matrix3x3 [1]) { { TAIT_BRYAN_XYZ_EXTRINSIC(1.5708, 9.2076e-28, -8.2157e-15) } },
            .translation = (struct vector3 [1]) {0.0, 0.00017505, -0.10593},
        },
        .joint = {
            .type = JOINT_TYPE_REVOLUTE,
            .revolute_joint = {
                .axis = JOINT_AXIS_Z,
                .inertia = (double[]) { 0.0 }
            }
        },
        .link = {
            .inertia = {
                .zeroth_moment_of_mass = 0.6781,
                .first_moment_of_mass = {6.781e-07, -0.030842022300000004, -0.006543665000000001},
                .second_moment_of_mass = {
                    .row_x = { 0.0011127621, 0.0, 0.0 },
                    .row_y = { 0.0, 0.000278021, -0.00018851179999999999 },
                    .row_z = { 0.0, -0.00018851179999999999, 0.0011127621 }
                }
            },
        }
    },
    {
        .joint_attachment = {
            .rotation = (struct matrix3x3 [1]) { { TAIT_BRYAN_XYZ_EXTRINSIC(-1.5708, -5.5511e-17, 9.6396e-17) } },
            .translation = (struct vector3 [1]) {0.0, -0.10593, -0.00017505},
        },
        .joint = {
            .type = JOINT_TYPE_REVOLUTE,
            .revolute_joint = {
                .axis = JOINT_AXIS_Z,
                .inertia = (double[]) { 0.0 }
            }
        },
        .link = {
            .inertia = {
                .zeroth_moment_of_mass = 0.5006,
                .first_moment_of_mass = {-0.0001406686, -0.005707841200000001, -0.014916878800000002},
                .second_moment_of_mass = {
                    .row_x = { 0.0002938522, 1.5018000000000002e-06, 1.5018000000000002e-06 },
                    .row_y = { 1.5018000000000002e-06, 0.00018472140000000002, 5.9070800000000004e-05 },
                    .row_z = { 1.5018000000000002e-06, 5.9070800000000004e-05, 0.0003048654 }
                }
            },
        }
    },
};

struct kca_kinematic_chain kinova_gen3_a = {
    .number_of_segments = 7,
    .segment = (struct kca_segment *)kinova_gen3_segments_a
};

struct kcc_kinematic_chain kinova_gen3_c = {
    .number_of_segments = 7,
    .segment = (struct kcc_segment *)kinova_gen3_segments_c
};
