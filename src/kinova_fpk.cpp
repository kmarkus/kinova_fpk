// SPDX-License-Identifier: LGPL-3.0
//
// kinova forward position kinematics block
// mostly copied from sim_kinova_gen3_bullet.c
//

#include "kinova_fpk.hpp"

#include <vis2b/functions/osg.h>
#include <ctrl2b/functions/ctrl2b.h>
#include <dyn2b/functions/geometry.h>
#include <dyn2b/functions/kinematic_chain.h>
#include <dyn2b/types/solver_state.h>
#include <sim2b/functions/bullet.h>

#include "kinova_gen3.h"

#include <math.h>

#define NR_JOINTS 7

def_port_writers(write_gc_pose, struct gc_pose);

struct kinova_fpk_info
{
    double jnt_pos[NR_JOINTS];

    struct gc_pose *x_tot;
    struct gc_pose *x_jnt;
    struct gc_pose *x_rel;

    struct kcc_kinematic_chain *kc;
    struct solver_state_c s;

    struct kinova_fpk_port_cache ports;
};

struct gc_pose* pose_alloc(int dim)
{
    struct gc_pose *p;
    struct matrix3x3 *r;
    struct vector3 *v;

    p = (struct gc_pose*) calloc(dim, sizeof(struct gc_pose));
    r = (struct matrix3x3*) calloc(dim, sizeof(struct matrix3x3));
    v = (struct vector3*) calloc(dim, sizeof(struct vector3));

    if (!p || !r || !v)
        goto out_err;

    for (int i=0; i<dim; i++) {
        p[i].rotation = &r[i];
        p[i].translation = &v[i];
    }

    return p;

out_err:
    if (!p) free(p);
    if (!r) free(r);
    if (!v) free(v);

    return NULL;
}

void pose_free(struct gc_pose *p)
{
    free(p->rotation);
    free(p->translation);
    free(p);
}



void fpk(
    struct kcc_kinematic_chain *kc,
    struct solver_state_c *s)
{
    for (int i = 1; i < s->nbody + 1; i++) {
        struct kcc_joint *joint = &kc->segment[i - 1].joint;
        int joint_type = joint->type;

        // Position
        //

        // X_{J,i}
        kcc_joint[joint_type].fpk(joint, &s->q[i - 1], &s->x_jnt[i - 1]);

        // i^X_{i-1} = X_{J,i} X_{T,i}
        gc_pose_compose(&s->x_jnt[i - 1], &kc->segment[i - 1].joint_attachment, &s->x_rel[i - 1]);

        // i^X_0 = i^X_{i-1} {i-1}^X_0
        gc_pose_compose(&s->x_rel[i - 1], &s->x_tot[i - 1], &s->x_tot[i]);
    }
}


/* init */
int kinova_fpk_init(ubx_block_t *b)
{
    int ret = -1;
    struct kinova_fpk_info *inf;

    /* allocate memory for the block local state */
    if ((inf = (struct kinova_fpk_info*) calloc(1, sizeof(struct kinova_fpk_info)))==NULL) {
        ubx_err(b, "kinova_fpk: failed to alloc memory");
        ret=EOUTOFMEM;
        goto out;
    }

    b->private_data = inf;
    inf->kc = &kinova_gen3_c;

    inf->x_tot = pose_alloc(NR_JOINTS+1);

    inf->x_tot[0].rotation->row_x.x = 1.0;
    inf->x_tot[0].rotation->row_y.y = 1.0;
    inf->x_tot[0].rotation->row_z.z = 1.0;

    inf->x_jnt = pose_alloc(NR_JOINTS);
    inf->x_rel = pose_alloc(NR_JOINTS);

    // Large actuators
    for (int i = 0; i < 4; i++) *inf->kc->segment[i].joint.revolute_joint.inertia = 0.5580; // kg*m^2

    // Small actuators
    for (int i = 4; i < 7; i++) *inf->kc->segment[i].joint.revolute_joint.inertia = 0.1389; // kg*m^2

    /* setup solver state */
    inf->s.nbody = NR_JOINTS;
    inf->s.nq = NR_JOINTS;

    inf->s.q = inf->jnt_pos;
    inf->s.x_jnt = inf->x_jnt;
    inf->s.x_rel = inf->x_rel;
    inf->s.x_tot = inf->x_tot;

    update_port_cache(b, &inf->ports);
    ret = 0;

out:
    return ret;
}


void kinova_fpk_cleanup(ubx_block_t *b)
{
    struct kinova_fpk_info *inf = (struct kinova_fpk_info*) b->private_data;

    pose_free(inf->x_tot);
    pose_free(inf->x_jnt);
    pose_free(inf->x_rel);

    free(b->private_data);
}


void kinova_fpk_step(ubx_block_t *b)
{
    long len;

    struct kinova_fpk_info *inf = (struct kinova_fpk_info*) b->private_data;

    len = read_double_array(inf->ports.jnt_pos, inf->jnt_pos, NR_JOINTS);

    if(len == 0) {
        ubx_notice(b, "unexpected NODATA on port jnt_pos");
    } else if (len != NR_JOINTS) {
        ubx_err(b, "jnt_pos has wrong length (got: %lu, expected: %i)", len, NR_JOINTS);
        return;
    }

    fpk(inf->kc, &inf->s);

    write_gc_pose_array(inf->ports.poses, inf->x_tot+1, NR_JOINTS);
}
