#ifndef DYN2B_ROBOT_KINOVA_GEN3
#define DYN2B_ROBOT_KINOVA_GEN3

#include <dyn2b/types/kinematic_chain.h>

#ifdef __cplusplus
extern "C" {
#endif

extern struct kca_kinematic_chain kinova_gen3_a;
extern struct kcc_kinematic_chain kinova_gen3_c;

#ifdef __cplusplus
}
#endif

#endif
